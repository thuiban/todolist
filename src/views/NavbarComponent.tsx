import React from "react";
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
     } from 'reactstrap';
import useRouter from "../services/useRouter";
import {makeStyles} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons";
import '../App.css'

const useStyles = makeStyles(theme => ({
    btn: {
        outline: 'none',
        border: 'none',
        backgroundColor: 'transparant',
        cursor: 'pointer',
    },

    "bg-white": {
        backgroundColor: 'indigo'
    }
}));

/**
 * @desc Barre de navigation en haut de page
 * @constructor
 */
function   NavbarComponent() {
      let logoutAction = (e: any) => {
        localStorage.clear();
       }
        const classes = useStyles();
        const history = useRouter();
        const currentRoute = history.location.pathname;
        const noDisplayNavBar = ['/signup', '/signin', '/forgot'];
        if (!noDisplayNavBar.includes(currentRoute)) {
            let Title = "To Do List : ";
            let homeLink = "";
            let userName = localStorage.getItem('username');
            if (localStorage.getItem('isAdmin') === 'true') {
                Title += "Tableau de bord - Admin";
                homeLink = "/admin/dashboard";
            } else  {
                Title += "Tableau de bord";
                homeLink = "/dashboard"
            }


            return (
                <div>
                    <Navbar color="blue" light  expand="md">
                        <NavbarBrand href={homeLink}><h3>{Title}</h3><h5> Bienvenue : {userName}</h5></NavbarBrand>
                              <Nav className="ml-auto" navbar >
                                <NavItem>
                                    <NavLink href="/swagger">API-Swagger</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="/signin" onClick={logoutAction} className={classes.btn}> <FontAwesomeIcon icon={faSignOutAlt} size="1x"/></NavLink>
                                </NavItem>
                            </Nav>
                    </Navbar>
                </div>
            );
        }
        return (<div></div>);
}

export default NavbarComponent;
