import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core";
import UserElem from "../../app/Manager/Admin/userElem";
import AdminApi from "../../services/Api/AdminApi";
import NotificationService from "../../services/NotificationService";
import useRouter from "../../services/useRouter";

const useStyles = makeStyles(theme => ({
    row: {
        display: 'flex',
        maxWidth: '100%',
        flexWrap: 'wrap',
    },
    dashboard: {
        textAlign: 'center',
        margin: '0 auto',
        width: '70%',
        display: 'block',
        boxShadow: '0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12)',
    },
    select: {
        marginBottom: "10%",
    },

    header: {
        height: '15%',
        display: 'inline-flex'
    },
    username: {
        marginRight: '10%'
    },
    title: {
        marginRight: '300%'

    }, logout: {},

    userlist: {
        marginLeft: 'auto',
        marginRight: 'auto',
    }
}));

/**
 * @desc Page principale de l'administrateur, affiche la liste des utilisateurs et de leurs tâches
 * @constructor
 */
function AdminDashboard() {
    const classes = useStyles();
    const notificationService = new NotificationService();
    const router = useRouter();


    const redirect = function (error) {
        localStorage.clear();
        router.history.push('/signin');
        notificationService.Erreur("Veuillez vous re-connecter.")
    };

    const [userList, setUserList] = useState([
        {id: -1, username: '', email: "", ban: "", admin : ""},
    ]);
    const AdminService = new AdminApi();

    // eslint-disable-next-line
    useEffect(() => {
        AdminService.getUsersData().then((data) => setUserList(data.data)).catch(redirect);

    }, []);
    return (
        <div>
            <div className={classes.dashboard}>
                {userList.map(d => <UserElem key={d.id} id={d.id} username={d.username} email={d.email} enabled={d.ban} isAdmin={d.admin}/>)}
            </div>
        </div>
    );
}

export default AdminDashboard;
