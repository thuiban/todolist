import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import SignInForm from "../../app/Authentication/signin/SignInForm";

/**
 * @desc Page de connexion
 */
const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: '#282c34',
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#C4C4C4',
        padding: '40px',
        borderRadius: '15px',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
}));

/**
 * @desc Application Registration Form
 */
function SignIn() {
    const classes = useStyles();

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Connexion
                </Typography>
                <SignInForm/>
            </div>
        </Container>
    );
}

export default SignIn;
