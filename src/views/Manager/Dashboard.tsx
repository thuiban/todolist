import React from "react";
import TaskList from "../../app/Manager/TaskList";
import useRouter from "../../services/useRouter";

/**
 * @desc Page principale de l'utilsiateur qui contient la liste de ses tâches
 * @constructor
 */
function UserDashboard() {
    const router = useRouter();

    if (localStorage.getItem('isAdmin') === 'true') {
        router.history.push('/admin/dashboard');
    }

    return (
        <div>
            <TaskList/>
        </div>
    );
}

// @ts-ignore
export default UserDashboard;
