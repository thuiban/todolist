import axios from 'axios';
// @ts-ignore
import oauth from 'axios-oauth-client';

// @ts-ignore

class AuthenticationService {
    public async login(username: string, password: string) {
        let getOwnerCredentials = oauth.client(axios.create(), {
            url: process.env.REACT_APP_API_URL+"/oauth/v2/token",
            grant_type: process.env.REACT_APP_GRANT_TYPE,
            client_id: process.env.REACT_APP_CLIENT_ID,
            client_secret: process.env.REACT_APP_CLIENT_SECRET,
            username: username,
            password: password,

        });
       return await getOwnerCredentials();
    }
}

export default AuthenticationService
