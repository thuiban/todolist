import {AxiosRequestConfig} from 'axios';
import Axios from "./Axios";

class AdminApi {
    config: AxiosRequestConfig;
    constructor() {
        this.config = {
            headers : {
                'Authorization' : "Bearer " + localStorage.getItem('bearer'),
                'Access-Control-Allow-Origin': '*'
            }
        }
    }

    getUsersData() {
        return Axios.get(process.env.REACT_APP_API_URL + "/api/admin/users", this.config)
    }

    editUserData(userId, data){
        return Axios.patch(process.env.REACT_APP_API_URL + "/api/admin/user/" + userId, data, this.config);
    }

    getUserTasksData(userId, isDone = -1) {
        return Axios.get(process.env.REACT_APP_API_URL + "/api/admin/tasks/" + userId + "?done=" + isDone, this.config)
    }

    banUser(userId, data) {
        return Axios.patch(process.env.REACT_APP_API_URL + "/api/admin/user/ban/" + userId, data, this.config);
    }

    deleteUser(userId) {
        return Axios.delete(process.env.REACT_APP_API_URL + "/api/admin/user/delete/" + userId, this.config);
    }

    postUserTask(userId, data) {
        return Axios.post(process.env.REACT_APP_API_URL + "/api/admin/user/" + userId + "/task", data, this.config);
    }

    patchUserTask(userId, taskId, data) {
        return Axios.patch(process.env.REACT_APP_API_URL + "/api/admin/user/" + userId + "/task/" + taskId, data, this.config);
    }

    deleteUserTask(userId, taskId) {
        return Axios.delete(process.env.REACT_APP_API_URL + "/api/admin/user/" + userId + "/task/" + taskId, this.config);
    }
}

export default AdminApi
