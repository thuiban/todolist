import {AxiosRequestConfig} from 'axios';
import Axios from "./Axios";

class UserApi {
    config: AxiosRequestConfig;
    constructor() {
         this.config = {
            headers : {
                        'Authorization' : "Bearer " + localStorage.getItem('bearer'),
                        'Access-Control-Allow-Origin': '*'
            }
        }
    }

    getUserData() {
        return Axios.get(process.env.REACT_APP_API_URL + "/api/user", this.config)
    }

    postRegister(data) {
        return Axios.post(process.env.REACT_APP_API_URL + "/api/register", data);
    }

    postForgotPassword(data) {
        return Axios.post(process.env.REACT_APP_API_URL + "/api/password_forget", data)
    }

}

export default UserApi
