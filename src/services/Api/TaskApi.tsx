import {AxiosRequestConfig} from 'axios';
import Axios from "./Axios";

class TaskApi {
    config: AxiosRequestConfig;
    constructor() {
        this.config = {
            headers : {
                'Authorization' : "Bearer " + localStorage.getItem('bearer'),
                'Access-Control-Allow-Origin': '*'
            }
        }
    }

    getTasksData(isDone = -1) {
        return Axios.get(process.env.REACT_APP_API_URL + "/api/tasks?done=" + isDone, this.config);
    }

    postTask(data) {
        return Axios.post(process.env.REACT_APP_API_URL + "/api/task", data, this.config);
    }

    getTaskData(id) {
        return Axios.get(process.env.REACT_APP_API_URL + "/api/task/" + id, this.config);
    }

    deleteTask(id) {
        return Axios.delete(process.env.REACT_APP_API_URL + "/api/task/" + id, this.config);
    }

    patchTask(id, data) {
        return Axios.patch(process.env.REACT_APP_API_URL + "/api/task/" + id, data, this.config);
    }
}

export default TaskApi
