import { store } from "react-notifications-component";

class NotificationService {
    Succes(message)
    {
        store.addNotification({
            title: "Succès",
            message: message,
            insert:'top',
            type: 'success',
            container: 'top-right',
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: 5000,
            }

        })
    }

    Erreur(message)
    {
        store.addNotification({
            title: "Erreur",
            message: message,
            insert:'top',
            type: 'danger',
            container: 'top-right',
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: 5000,
            }
        })
    }
}

export default NotificationService
