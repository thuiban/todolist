import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import TaskList from "../TaskList";
import AdminApi from "../../../services/Api/AdminApi";
import NotificationService from "../../../services/NotificationService";
import useRouter from "../../../services/useRouter";
import BaseModal from "../BaseModal";
import {makeStyles} from "@material-ui/core";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import CircularProgress from "@material-ui/core/CircularProgress";


const useStyles = makeStyles(theme => ({
    table: {
        margin: '0 auto',
    },
    td: {
        minWidth: '20%',
    }
}));

/**
 * @desc Contient tout les éléments liés à un utilisateur affichés sur l'interface d'administration
 * @param props
 * @constructor
 */
function UserElem(props: any) {
    const classes = useStyles();

    const AdminService =  new AdminApi();
    const notificationService = new NotificationService();
    const [loading, setLoading] = useState(0);
    const [loading2, setLoading2] = useState(0);
    const router = useRouter();
    const [isShowing, setShowing] = useState(false);
    const [data] = useState({id: props.id , username: props.username, email: props.email, isAdmin: props.isAdmin});
    const [type, setType] = useState("add");

    /**
     * Suppression d'un utilisateur
     */
    const deleteUser = () => {
        setLoading2(1);
        AdminService.deleteUser(props.id).then(function (data) {
            notificationService.Succes("L'utilisateur à été supprimé  avec succès");
            router.history.push('/');
            setLoading(0);
        }).catch(function (error) {
            // eslint-disable-next-line no-useless-escape
            notificationService.Erreur("L\'action en vers l\'utilisateur n\'a pas pus être réalisé");
        });
    };

    /**
     * @desc Banissement d'un utilisateur
     */
    const banUser = () => {
        setLoading(1);
        let active = true;
        if (props.enabled === true)
        {
            active = false;
        }
        const data = {
            ban : active,
        }

        AdminService.banUser(props.id, data).then(function (data) {
            const isBan = props.enabled === true ? "banni" : "débanni";
            notificationService.Succes("L'utilisateur à été " + isBan + " avec succès");
            setLoading(0);
            router.history.push('/')
        }).catch(function (error) {
            // eslint-disable-next-line no-useless-escape
            notificationService.Erreur("L\'action en vers l\'utilisateur n\'a pas pus être réalisé");
        });
    };

    /**
     * @desc Event d'ouverture de la modale
     */
    const openModalHandler = () => {
        setShowing(true);
        setType("user");
    };

    /**
     * @desc Event de fermeture de la modale
     */
    const closeModalHandler = () => {
        setShowing(false);
        setType("detail");
    };

    const isBan = props.enabled === true ? "Bannir" : "Débannir";
    return (
        <div>
        <table className={classes.table}>
            <tbody>
                <tr>
                    <td>
                        <div className={classes.td}>
                            {props.isAdmin && <FontAwesomeIcon icon={faStar} color="black" size="1x"/>} {props.username}
                        </div>
                    </td>
                    <td>
                        {props.email}
                    </td>
                    <td>
                        <Button variant="contained" onClick={openModalHandler}>
                            Editer
                        </Button>
                    </td>
                    <td>
                        <Button variant="contained" onClick={deleteUser}>
                            {loading2 === 0 &&
                            "Supprimer"}
                            {loading2 === 1 &&
                                <CircularProgress/>
                            }
                        </Button>
                    </td>
                    <td>
                        <Button variant="contained" onClick={banUser}>
                            {loading === 0 &&
                            isBan}
                            {loading === 1 &&
                            <CircularProgress/>
                            }
                        </Button>
                    </td>
                </tr>
                <tr>
                    <td colSpan={5}><TaskList userId={props.id}/></td>
                </tr>
            </tbody>
        </table>
            {isShowing && <BaseModal
                className="modal"
                show={isShowing}
                close={closeModalHandler}
                type={type}
                data={data}
                setType={setType}>
            </BaseModal>}
        </div>
    );
}

export default UserElem;
