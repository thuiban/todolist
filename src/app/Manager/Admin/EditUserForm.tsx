import {makeStyles} from "@material-ui/core";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import React, {useEffect, useState} from "react";
import NotificationService from "../../../services/NotificationService";
import useRouter from "../../../services/useRouter";
import CircularProgress from "@material-ui/core/CircularProgress";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import AdminApi from "../../../services/Api/AdminApi";

const useStyles = makeStyles(theme => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    TextField: {
        backgroundColor: 'white',
    }
}));

/**
 * @Êesc Formulaire d'édition d'un profil utilisateur
 * @param props
 * @constructor
 */
function EditUserForm(props: any) {
    const classes = useStyles();
    const [loading, setLoading] = useState(0);
    const [userData, setUserData] = useState({username: "", email: "", isAdmin: false});
    const notficationService = new NotificationService();
    const AdminService = new AdminApi();
    const router = useRouter();

    useEffect(() => {
        setUserData({username: props.data.username, email: props.data.email, isAdmin: props.data.isAdmin});
        }, [props.data.username, props.data.email, props.data.isAdmin]);

    /**
     * @desc Event d'envois du formulaire
     * @param e
     */
    let submit = (e: any) => {
        e.preventDefault();
        setLoading(1);
        AdminService.editUserData(props.data.id, {
           username: userData.username,
           email: userData.email,
           admin: userData.isAdmin
        }).then(function (data) {
            setLoading(0);
            notficationService.Succes("L'utilisateur à été modifié  avec succès");
            router.history.push('/')
        }).catch(function (error) {
            // eslint-disable-next-line no-useless-escape
            setLoading(0);
            notficationService.Erreur("L'action n'a pas pus être réaliséé");
        });
    };

    return  (
        <ValidatorForm
            onSubmit={submit}
            onError={errors => console.log(errors)}
        >
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextValidator
                        fullWidth
                        variant="outlined"
                        label="Email"
                        onChange={event => setUserData({username: userData.username, email: (event.target as HTMLInputElement).value, isAdmin: userData.isAdmin})}
                        name="email"
                        value={userData.email}
                        validators={['required']}
                        errorMessages={['Ce champ est requis']}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextValidator
                        fullWidth
                        variant="outlined"
                        label="Nom d'utilisateur"
                        onChange={event => setUserData({username: (event.target as HTMLInputElement).value, email: userData.email, isAdmin: userData.isAdmin})}
                        name="username"
                        value={userData.username}
                        validators={['required']}
                        errorMessages={['Ce champ est requis']}
                    />
                </Grid>
                <Grid item xs={12}>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={userData.isAdmin}
                                value="isAdmin"
                                onChange={event => setUserData({username: userData.username, email: userData.email, isAdmin: event.target.checked})}
                            />
                        }
                        label="Administrateur" />
                </Grid>
            </Grid>
            {loading === 1 && <CircularProgress />}
            {loading === 0 &&
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Valider
            </Button>}
        </ValidatorForm>
    );
}

export default EditUserForm;
