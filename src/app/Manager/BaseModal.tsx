import React from 'react';
import {makeStyles} from "@material-ui/core";
import AddForm from "./add/AddForm";
import Detail from "./detail/Detail";
import Button from '@material-ui/core/Button';
import EditUserForm from "./Admin/EditUserForm";

const useStyles = makeStyles(theme => ({
    modalWrapper: {
        background: 'white',
        border: '1px solid #d0cccc',
        boxShadow: '0 5px 8px 0 rgba(0,0,0,0.2), 0 7px 20px 0 rgba(0,0,0,0.17)',
        margin: '100px auto 0',
        transition: 'all .8s',
        width: '60%',
    },

    modalHeader: {
        backgroundColor: '#263238',
        height: '40px',
        lineHeight: '40px',
        padding: '5px 20px',
        textAlign: 'right',
    },

    h3: {
        color: 'white',
        float: 'left',
        margin: '0',
        padding: '0',
    },

    modalBody: {
        padding: '10px 15px',
        textAlign: 'center',
    },

    closeModalBtn: {
        color: 'white',
        cursor: 'pointer',
        float: 'right',
        fontSize: '30px',
        margin: '0',
    },

    editButton: {
        float: 'left',
    }
}));

/**
 * @desc Base de la modale utilisée pour l'ajout/édition/affichage de tâches et l'édition d'un utilisateur
 * @param props
 * @constructor
 */
function BaseModal(props: any) {
    const classes = useStyles();

    /**
     * @desc Permet de passer la modale en mode d'édition de tâche
     */
    const showEdit = () => {
        props.setType("edit");
    };

    return (
        <div className={classes.modalWrapper}
             style={{
                 transform: props.show ? 'translateY(-20vh)' : 'translateY(-500vh)',
                 opacity: props.show ? '1' : '0'
             }}>
            <div className={classes.modalHeader}>
                {props.type === "add" &&  <h3 className={classes.h3}>Ajouter une tâche</h3>}
                {props.type === "detail" &&  <h3 className={classes.h3}>Détail de la tâche</h3>}
                {props.type === "edit" &&  <h3 className={classes.h3}>Edition de la tâche</h3>}
                {props.type === "user" &&  <h3 className={classes.h3}>Edition de l'utilisateur</h3>}
                <span className={classes.closeModalBtn} onClick={props.close}>×</span>
            </div>
            {props.type === "detail" && <Button variant="contained" className={classes.editButton} onClick={showEdit}>Edition</Button>}
            <div className={classes.modalBody}>
                {props.type === "add" && <AddForm data={props.data} userId={props.userId} type={props.type}/>}
                {props.type === "detail" && <Detail data={props.data}/>}
                {props.type === "edit" && <AddForm data={props.data}  userId={props.userId} type={props.type}/>}
                {props.type === "user" && <EditUserForm data={props.data}/>}
            </div>
        </div>
    )
}

export default BaseModal;
