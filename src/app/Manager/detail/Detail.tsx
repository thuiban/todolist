import React from "react";

/**
 * @desc Affiche les informations de la tâche dans la modale
 * @param props
 * @constructor
 */
function Detail(props: any) {
    return  (
        <div>
            <h3>{props.data.titre}</h3>
            <p>{props.data.description}</p>
            <p>{props.data.date}</p>
        </div>
    );
}

export default Detail;
