import React from 'react';
import {makeStyles} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheckSquare} from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles(theme => ({
    btn: {
        outline: 'none',
        border: 'none',
        backgroundColor: 'white',
        cursor: 'pointer',
    },
}));

/**
 * @desc Bouton permettant de valider ou de d'annuler la validation d'une tâche
 * @param props
 * @constructor
 */
function DoneButton(props: any) {
    const classes = useStyles();

    return (
        <button className={classes.btn} onClick={props.doneHandler}>
            {props.status === "checked" && <FontAwesomeIcon icon={faCheckSquare} color="green" size="2x"/>}
            {props.status === "unchecked" && <FontAwesomeIcon icon={faCheckSquare} color="grey" size="2x"/>}
        </button>
    );
}

export default DoneButton;
