import React, {useEffect, useState} from "react";
import Sticker from "../../app/Manager/Sticker"
import {makeStyles} from "@material-ui/core";
import BaseModal from "../../app/Manager/BaseModal";
import {FormControl, MenuItem, Select} from "@material-ui/core";
import TaskApi from "../../services/Api/TaskApi";
import NotificationService from "../../services/NotificationService";
import AdminApi from "../../services/Api/AdminApi";
import useRouter from "../../services/useRouter";
import CircularProgress from "@material-ui/core/CircularProgress";


const useStyles = makeStyles(theme => ({
    row: {
        display: 'flex',
        maxWidth: '100%',
        flexWrap: 'wrap',
    },
    tasklist: {
        textAlign: 'center',
        margin: '0 auto',
        width: '85%',
        display: 'block',
    },
    select: {
        marginBottom: "10%",
    },
}));

/**
 * @desc Affiche la liste des tâches d'un utilisateur
 * @param props
 * @constructor
 */
function TaskList(props: any) {
    const classes = useStyles();
    const notificationService = new NotificationService();
    const [isShowing, setShowing] = useState(false);
    const [loading, setLoading] = useState(1);
    const [type, setType] = useState("add");
    const [data, setData] = useState({titre: "", description: "", date: ""});
    const [selected, setSelected] = useState('all');
    const router = useRouter();
    const [stickerList, setStickerList] = useState([
        {id: -1, titre: '', date: "", done: 0, description: ""},

    ]);

    const redirect = function (error) {
        localStorage.clear();
        router.history.push('/signin');
        notificationService.Erreur("Veuillez vous re-connecter.")
    };
    const TaskService = new TaskApi();
    const AdminService = new AdminApi();

    // eslint-disable-next-line
    useEffect(() => {
        if (localStorage.getItem('isAdmin') === 'false') {
            TaskService.getTasksData().then((data) => setStickerList(data.data));
            TaskService.getTasksData()
                .then((data) => {setStickerList(data.data); setLoading(0);})
                .catch(redirect);
        } else {
            AdminService.getUserTasksData(props.userId).then((data) => setStickerList(data.data));
            AdminService.getUserTasksData(props.userId)
                .then((data) => {setStickerList(data.data);setLoading(0);})
                .catch(redirect);
        }
    },[]);

    /**
     * @desc Event d'ouverture de la modale
     */
    const openModalHandler = () => {
        setData({titre: "", description: "", date: ""});
        setType("add");
        setShowing(true);
    };

    /**
     * @desc Event permettant d'afficher les détails d'une tâche sur la modale
     * @param id
     */
    const detailsHandler = (id: number) => {

        TaskService.getTaskData(id).then(function (data) {
            setData(data.data);
            setType("detail");
            setShowing(true);
        }).catch(function (error) {
            notificationService.Erreur("Un problème est survenue lors de la récupération de la page");
        })
    };

    /**
     * @desc Event de fermeture de la modale
     */
    const closeModalHandler = () => {
        setShowing(false);
        setType("detail");
    };

    const handleChange = (value: any) => {
        let isDone = -1;
        if (value === "checked") {
            isDone = 1;
        } else if (value === "unchecked") {
            isDone = 0;
        }
        setSelected(value);
        if (localStorage.getItem('isAdmin') === 'false') {
            TaskService.getTasksData(isDone).then((data) => setStickerList(data.data));
        } else {
            AdminService.getUserTasksData(props.userId, isDone).then((data) => setStickerList(data.data));
        }
    };

    return (
        <div className={classes.tasklist}>
            <FormControl>
                <Select
                    id="sort-type"
                    onChange={event => handleChange((event.target as HTMLInputElement).value)}
                    placeholder="tout"
                    value={selected}
                    className={classes.select}
                >
                    <MenuItem value="all">Toutes les tâches</MenuItem>
                    <MenuItem value="checked">Tâches terminées</MenuItem>
                    <MenuItem value="unchecked">Tâches non terminées</MenuItem>
                </Select>
            </FormControl>
            {loading === 1 &&
            <div className={classes.row}>
                <CircularProgress />
            </div>}
            {loading === 0 &&
            <div className={classes.row}>
                {stickerList.map(d => <Sticker key={d.id} id={d.id} title={d.titre} date={d.date} done={d.done} description={d.description} detailsHandler={detailsHandler} userId={props.userId} />)}
                <Sticker add={true} openModalHandler={openModalHandler}/>
            </div>}
            {isShowing && <BaseModal
                className="modal"
                userId={props.userId}
                show={isShowing}
                close={closeModalHandler}
                type={type}
                data={data}
                setType={setType}>
            </BaseModal>}
        </div>
    );
}

export default TaskList;
