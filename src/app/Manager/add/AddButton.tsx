import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusSquare} from "@fortawesome/free-solid-svg-icons";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    btn: {
        outline: 'none',
        border: 'none',
        backgroundColor: 'white',
        cursor: 'pointer',
    },
}));

/**
 * @desc Bouton permettant d'ouvrir la modale d'ajout de t$ache
 * @param props
 * @constructor
 */
function AddButton(props: any) {
    const classes = useStyles();

    return (
        <button className={classes.btn} onClick={props.openModalHandler}>
            <FontAwesomeIcon icon={faPlusSquare} color="black" size="3x"/>
        </button>
    );
}

export default AddButton;
