import {makeStyles} from "@material-ui/core";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import React, {useState} from "react";
import DateFnsUtils from "@date-io/date-fns";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import moment from "moment";
import TaskApi from "../../../services/Api/TaskApi";
import NotificationService from "../../../services/NotificationService";
import useRouter from "../../../services/useRouter";
import CircularProgress from "@material-ui/core/CircularProgress";
import AdminApi from "../../../services/Api/AdminApi";

const useStyles = makeStyles(theme => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    TextField: {
        backgroundColor: 'white',
    }
}));

/**
 * @desc Formmulaire d'ajout d'une tâche
 * @param props
 * @constructor
 */
function AddForm(props: any) {
    const classes = useStyles();
    const date = props.data.date !== "" ? moment(props.data.date, "DD/MM/YYYY").toDate() : new Date();
    const [loading, setLoading] = useState(0);
    const [taskData, setTaskData] = useState({titre: props.data.titre, description: props.data.description, date: date});
    const TaskService = new TaskApi();
    const AdminService = new AdminApi();
    const NotficationService = new NotificationService();
    const router = useRouter();

    /**
     * @desc Met à jour la date suivant le choix dans le formulaire
     * @param date
     */
    // @ts-ignore
    const handleDateChange = date => {
        setTaskData({titre: taskData.titre, description: taskData.description, date: date});
    };

    /**
     * @desc Event d'envois du formulaire d'ajout d'une têche côté utilisateur
     * @param type
     */
    let userSubmit = (type: string) => {
        if (type === "add") {
            setLoading(1);
            TaskService.postTask({
                Title: taskData.titre,
                Description: taskData.description,
                DateRelease: taskData.date.toLocaleDateString(),
                Done: 0
            })
                .then(function (data) {
                    NotficationService.Succes("Votre tâche à été ajouté avec succès");
                    router.history.push('/');
                    setLoading(0);
                })
                .catch(function (data) {
                    NotficationService.Erreur("Erreur lors de l'enregistrement de votre tâche");
                    setLoading(0);
                });
        } else if (type === "edit") {
            setLoading(1);
            TaskService.patchTask( props.data.id, {
                Title: taskData.titre,
                Description: taskData.description,
                DateRelease: taskData.date.toLocaleDateString(),
                Done: 0
            })
                .then(function (data) {
                    NotficationService.Succes("Votre tâche à été mis à jour avec succès");
                    router.history.push('/');
                })
                .catch(function (data) {
                    NotficationService.Erreur("Erreur lors de la mise a jour de votre tâche");
                    setLoading(0);
                });
        }
    };

    /**
     * @desc Event d'envois du formulaire d'ajout d'une têche côté administrateur
     * @param type
     */
    let adminSubmit = (type: string) => {
        if (type === "add") {
            setLoading(1);
            AdminService.postUserTask(props.userId, {
                Title: taskData.titre,
                Description: taskData.description,
                DateRelease: taskData.date.toLocaleDateString(),
                Done: 0
            })
                .then(function (data) {
                    NotficationService.Succes("Votre tâche à été ajouté avec succès");
                    router.history.push('/');
                    setLoading(0);
                })
                .catch(function (data) {
                    NotficationService.Erreur("Erreur lors de l'enregistrement de votre tâche");
                    setLoading(0);
                });
        } else if (type === "edit") {
            AdminService.patchUserTask(props.userId,  props.data.id, {
                Title: taskData.titre,
                Description: taskData.description,
                DateRelease: taskData.date.toLocaleDateString(),
                Done: 0
            })
                .then(function (data) {
                    NotficationService.Succes("Votre tâche à été mis à jour avec succès");
                    router.history.push('/dashboard');
                })
                .catch(function (data) {
                    NotficationService.Erreur("Erreur lors de la mise a jour de votre tâche");
                });
        }
    };

    /**
     * @desc Choix du type d'envois suivant le type de compte connecté
     * @param e
     */
    let submit = (e: any) => {
        e.preventDefault();

        if (localStorage.getItem('isAdmin') === 'true') {
            adminSubmit(props.type)
        } else {
            userSubmit(props.type);
        }

    };

    return  (
        <ValidatorForm
            onSubmit={submit}
            onError={errors => console.log(errors)}
        >
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextValidator
                        fullWidth
                        variant="outlined"
                        label="Titre"
                        onChange={event => setTaskData({titre: (event.target as HTMLInputElement).value, description: taskData.description,
                            date: taskData.date})}
                        name="titre"
                        value={taskData.titre}
                        validators={['required']}
                        errorMessages={['Ce champ est requis']}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextValidator
                        fullWidth
                        variant="outlined"
                        label="Description"
                        onChange={event => setTaskData({titre: taskData.titre, description: (event.target as HTMLInputElement).value,
                            date: taskData.date})}
                        name="description"
                        value={taskData.description}
                        validators={['required']}
                        errorMessages={['Ce champ est requis']}
                    />
                </Grid>
                <Grid container justify="space-around" item xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="date-picker-inline"
                            label="Date"
                            value={taskData.date}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
            </Grid>
            {loading === 1 && <CircularProgress />}
            {loading === 0 &&
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Ajouter
            </Button>}
        </ValidatorForm>
    );
}

export default AddForm;
