import React, {useState} from 'react';
import {makeStyles} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Done from "./DoneButton";
import Add from "./add/AddButton";
import DeleteButton from "./DeleteButton";
import TaskApi from "../../services/Api/TaskApi";
import useRouter from "../../services/useRouter";
import NotificationService from "../../services/NotificationService";
import AdminApi from "../../services/Api/AdminApi";


const useStyles = makeStyles(theme => ({
    title: {
        textColor: 'black',
    },

    box: {
        minHeight: '100px',
        minWidth: '200px',
        marginLeft: '10px',
        marginBottom: '10px',
    },

    done: {
        float: 'right',

    },

    delete: {
        float: 'left',

    },

    add: {
        position: 'relative',
        top: '25%',

    },

    stickerData: {
        marginTop: "10%",
        marginBottom: "10%",
    },
}));

/**
 * @desc Vignette affichée dans la lsite des tâches, une vignette est générée par tâche
 * @param props
 * @constructor
 */
function Sticker(props: any) {
    const classes = useStyles();
    const [status, setStatus] = useState(props.done === 1 ? "checked" : "unchecked");
    const TaskService = new TaskApi();
    const AdminService = new AdminApi();
    const router = useRouter();
    const notificationService = new NotificationService();

    /**
     * @desc Event de validation de la tâche
     */
    const doneHandler = () => {
        let done;
        if (status === "unchecked") {
            setStatus("checked");
            done = 1;
        } else {
            setStatus("unchecked");
            done = 0;
        }
        let data = {
            Title: props.title,
            Description: props.description,
            DateRelease: props.date,
            Done: done
        };

        if (localStorage.getItem('isAdmin') === 'false') {
            TaskService.patchTask(props.id, data)
                .then(function (data) {
                    notificationService.Succes("La tâche à été mise à jour");
                    router.history.push("/dashboard");
                }).catch(function (error) {
                notificationService.Erreur("Un problème est survenue lors de la mise à jour de la tâche");
            });
        } else {
            AdminService.patchUserTask(props.userId, props.id, data).then(function (data) {
                notificationService.Succes("La tâche à été mise à jour");
                router.history.push("/admin/dashboard");
            }).catch(function (error) {
                notificationService.Erreur("Un problème est survenue lors de la mise à jour de la tâche");
            });
        }
    };

    /**
     * @desc Event de suppression de la tâche
     */
    const deleteHandler = () => {

        if (localStorage.getItem('isAdmin') === 'false') {
            TaskService.deleteTask(props.id)
                .then(function (data) {
                    notificationService.Succes("La tâche à été supprimé");
                    router.history.push("/");
                }).catch(function (error) {
                notificationService.Erreur("Un problème est survenue lors de la suppression de la tâche");
            });
        } else {
            AdminService.deleteUserTask(props.userId, props.id).then(function (data) {
                notificationService.Succes("La tâche à été supprimé");
                router.history.push("/");
            }).catch(function (error) {
                notificationService.Erreur("Un problème est survenue lors de la suppression de la tâche");
            });
        }
    };

    if (props.add) {
        return (
            <Box boxShadow={3} className={classes.box}>
                <div className={classes.add}>
                    <Add openModalHandler={props.openModalHandler}/>
                </div>
            </Box>
        );
    } else {
        return (
            <Box boxShadow={3} className={classes.box}>
                <div className={classes.delete}>
                    <DeleteButton deleteHandler={deleteHandler}/>
                </div>
                <div className={classes.done}>
                    <Done doneHandler={doneHandler} status={status}/>
                </div>
                <div className={classes.stickerData} onClick={() => props.detailsHandler(props.id)}>
                    <div className={classes.title}> {props.title} </div>
                    <div> {props.date}</div>
                </div>
            </Box>
        );
    }
}

export default Sticker;
