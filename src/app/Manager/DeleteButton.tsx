import React from 'react';
import {makeStyles} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles(theme => ({
    btn: {
        outline: 'none',
        border: 'none',
        backgroundColor: 'white',
        cursor: 'pointer',
    },
}));

/**
 * @desc Bouton de suppression d'une tâche
 * @param props
 * @constructor
 */
function DeleteButton(props: any) {
    const classes = useStyles();

    return (
        <button className={classes.btn} onClick={props.deleteHandler}>
            <FontAwesomeIcon icon={faTrashAlt} color="red" size="2x"/>
        </button>
    );
}

export default DeleteButton;
