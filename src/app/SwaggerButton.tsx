import React from 'react';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faDatabase} from "@fortawesome/free-solid-svg-icons";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    btn: {
        outline: 'none',
        border: 'none',
        backgroundColor: 'white',
        cursor: 'pointer',
    },
}));

/**
 * @desc Bouton permettant l'accès à la documentation API
 * @constructor
 */
function SwaggerButton() {
    const classes = useStyles();
    return (
        <Link to="/swagger" ><Button className={classes.btn}>
            <FontAwesomeIcon icon={faDatabase} size="2x"/>API Swagger
        </Button>
        </Link>
    );
}

export default SwaggerButton;
