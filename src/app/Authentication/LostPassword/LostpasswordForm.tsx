import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LinkCore from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { ValidatorForm } from "react-material-ui-form-validator";

import {
    Link,
} from "react-router-dom";
import UserApi from "../../../services/Api/UserApi";
import useRouter from "../../../services/useRouter";
import NotificationService from "../../../services/NotificationService";

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

/**
 * @desc Formulaire de mot de passe oublié
 * @constructor
 */
function LostPasswordForm() {
    const classes = useStyles();
    const router = useRouter();
    const userService = new UserApi();
    const notificationService = new NotificationService();


    const [email, setEmail] = useState('');

    /**
     * @desc d'envois du formulaire
     * @param e
     */
    let submitEvent = (e: any) => {
        userService.postForgotPassword({
            email: email
        }).then(function (data) {
            notificationService.Succes('Vous allez recevoir un mail avec votre nouveau mot de passe, si vous êtes inscrits');
            router.history.push("/signin");
        }).catch(function (data) {
                notificationService.Erreur("Un problème est survenu");
            })
    };

    return (
        <ValidatorForm
            onSubmit={submitEvent}
            className={classes.form} noValidate>
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Adresse Email"
                name="email"
                onChange = {event => setEmail((event.target as HTMLInputElement).value)}
                autoComplete="email"
                autoFocus
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Envoyer la demande de changement
            </Button>
            <Grid container>
                <Grid item>
                    <LinkCore variant="body2">
                        <Link to="/signin" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                            Connexion
                        </Link>
                    </LinkCore>
                </Grid>
            </Grid>
        </ValidatorForm>
    );
}

export default LostPasswordForm;
