import {makeStyles} from "@material-ui/core";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import React, {useState} from "react";
import UserApi from "../../../services/Api/UserApi";
import {
    Redirect
} from 'react-router-dom';

import useRouter from "../../../services/useRouter";
import NotificationService from "../../../services/NotificationService";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    loading: {
        margin: theme.spacing(3, 16, 2),
    },
    TextField: {
        backgroundColor: 'white',
    }
}));

/**
 * @desc Formulaire d'inscription
 * @constructor
 */
function SignUpForm(props: any) {
    const classes = useStyles();
    const userService = new UserApi();
    const notificationService = new NotificationService();
    const [loading, setLoading] = useState(0);
    const [userData, setUserData] = useState({username: '', email: '', password: ''});
    const [confirmPassword, setConfirmPassword] = useState('');
    const router = useRouter();

    /**
     * @desc Event d'envois du formulaire
     * @param e
     */
    let submit = async (e: any) => {
        e.preventDefault();

        if (userData.password === confirmPassword) {
            setLoading(1);
            userService.postRegister(userData)
                .then(function (data) {
                    notificationService.Succes("Votre inscription a été prise en compte");
                    router.history.push("/signin");
                })
                .catch(function (err) {
                    notificationService.Erreur("L'inscription n'a pas pu aboutir");
                    setLoading(0);
                });

        } else {
            notificationService.Erreur("Les mots de passe ne correspondent pas")
        }
    };

    return  (
    <ValidatorForm
        onSubmit={submit}
        onError={errors => console.log(errors)}
        onValid={<Redirect to="/signin"/>}
    >
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <TextValidator
                    fullWidth
                    variant="outlined"
                    label="Adresse Email"
                    onChange={event => setUserData({username: userData.username, email: (event.target as HTMLInputElement).value,
                        password: userData.password})}
                    name="email"
                    value={userData.email}
                    validators={['required', 'isEmail']}
                    errorMessages={['This field is required', 'email is not valid']}
                />
            </Grid>
            <Grid item xs={12}>
                <TextValidator
                    fullWidth
                    variant="outlined"
                    label="Nom d'utilisateur"
                    onChange={event => setUserData({username: (event.target as HTMLInputElement).value, email: userData.email,
                        password: userData.password})}
                    name="username"
                    value={userData.username}
                    validators={['required', 'maxStringLength:15', 'minStringLength:4']}
                    errorMessages={['This field is required', 'Max length: 15', 'Min length: 4']}
                />
            </Grid>
            <Grid item xs={12}>
                <TextValidator
                    fullWidth
                    variant="outlined"
                    type="password"
                    label="Mo de passe"
                    onChange={event => setUserData({username: userData.username, email: userData.email,
                        password: (event.target as HTMLInputElement).value})}
                    name="password"
                    value={userData.password}
                    validators={['required', 'maxStringLength:15', 'minStringLength:8']}
                    errorMessages={['This field is required', 'Max length: 15', 'Min length: 8']}
                />
            </Grid>
            <Grid item xs={12}>
                <TextValidator
                    fullWidth
                    variant="outlined"
                    type="password"
                    label="Confirmation du mot de passe"
                    onChange={event => setConfirmPassword((event.target as HTMLInputElement).value)}
                    name="confirmPassword"
                    value={confirmPassword}
                    validators={['required', 'maxStringLength:15', 'minStringLength:8']}
                    errorMessages={['This field is required', 'Max length: 15', 'Min length: 8']}
                />
            </Grid>
        </Grid>
        {loading === 1 && <CircularProgress className={classes.loading}/>}
        {loading === 0 &&
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
        >
            Inscription
        </Button>}
        <Grid container justify="flex-end">
            <Grid item>
                <Link href="/signin" variant="body2">
                    Vous avez déjà un compte? Connectez-vous
                </Link>
            </Grid>
        </Grid>
    </ValidatorForm>
);
}

export default SignUpForm;
