import React from 'react';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';

/**
 * @desc Bouton permettant de valider le formulaire d'inscription'
 * @constructor
 */
function SignupButton() {
    return (
      <Link to="/signup" ><Button variant="contained" color="primary">
        Sign Up
      </Button>
      </Link>
    );
}

export default SignupButton;
