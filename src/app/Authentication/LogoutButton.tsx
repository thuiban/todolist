import React from 'react';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    btn: {
        outline: 'none',
        border: 'none',
        backgroundColor: 'transparant',
        cursor: 'pointer',
    },
}));

/**
 * @desc Bouton permettant de déconnecter un utilisateur
 * @constructor
 */
function LogoutButton() {
    let logoutAction = (e: any) => {
       localStorage.clear();
    }
    const classes = useStyles();
    return (
        <Link to="/signin" ><Button className={classes.btn} onClick={logoutAction}>
            <FontAwesomeIcon icon={faSignOutAlt} size="2x"/>
        </Button>
        </Link>
    );
}

export default LogoutButton;
