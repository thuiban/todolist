import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LinkCore from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { ValidatorForm } from "react-material-ui-form-validator";
import AuthenticationService from "../../../services/Authentication";

import {
    Link,
} from "react-router-dom";
import UserApi from "../../../services/Api/UserApi";
import NotificationService from "../../../services/NotificationService";
import useRouter from "../../../services/useRouter";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    loading: {
        margin: theme.spacing(3, 16, 2),
    },
}));

/**
 * @desc Formulaire de connexion
 * @constructor
 */
function SignInForm() {
    const classes = useStyles();
    const router = useRouter();
    const [userData, setUserData] = useState({username: '', password: ''});
    const [loading, setLoading] = useState(0);
    const notificationService = new NotificationService();

    /**
     * @desc Event d'envois du formulaire
     * @param e
     */
    let submitEvent = (e: any) => {
        setLoading(1);
        let credential = new AuthenticationService();
         credential.login(userData.username, userData.password)
             .then(function (data) {
                 localStorage.setItem('bearer', data.access_token);
                 localStorage.setItem('refresh', data.refresh_token);
                 let userApi = new UserApi();
                 userApi.getUserData()
                     .then(function (data) {
                         localStorage.setItem('username', data.data.username);
                         localStorage.setItem('isAdmin', data.data.isAdmin);
                         if (data.data.isAdmin) {
                            router.history.push('/admin/dashboard')
                         } else {
                             router.history.push('/dashboard');
                         }
                         return;
                     })
                     .catch(function (error) {
                         console.log(error);
                         notificationService.Erreur("Erreur durant la récuperation des données");
                         setLoading(0);
                         localStorage.clear();
                         router.history.push('/signin');
                     })

             })
             .catch(function (error: any) {
                 console.log(error);
                 notificationService.Erreur("L'authentification a échoué. Merci de vérifier vos informations.");
             });

    };

    if (localStorage.getItem('bearer'))
    {
        if (localStorage.getItem('isAdmin') === 'true')
        {
            router.history.push('/admin/dashboard')
        } else {
            router.history.push('/dashboard');
        }
    }

    return (
        <ValidatorForm
            onSubmit={submitEvent}
            className={classes.form} noValidate>
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Nom d'utilisateur"
                name="username"
                onChange = {event => setUserData({username: (event.target as HTMLInputElement).value, password: userData.password})}
                autoComplete="email"
                autoFocus
            />
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                onChange = {event => setUserData({username: userData.username, password: (event.target as HTMLInputElement).value})}
                label="Mot de passe"
                type="password"
                id="password"
                autoComplete="current-password"
            />
            {loading === 1 && <CircularProgress className={classes.loading}/>}
            {loading === 0 &&
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Connexion
            </Button>}
            <Grid container>
                <Grid item>
                    <LinkCore variant="body2">
                        <Link to="/forgot" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                            Mot de passe oublié?
                        </Link>
                    </LinkCore>
                </Grid>
                <Grid item>
                    <LinkCore variant="body2">
                        <Link to="/signup" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                            {"Vous n'avez pas de compte? Inscrivez-vous"}
                        </Link>
                    </LinkCore>
                </Grid>
            </Grid>
        </ValidatorForm>
    );
}

export default SignInForm;
