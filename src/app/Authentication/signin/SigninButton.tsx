import React from 'react';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';

/**
 * @desc Bouton permettant de valider le formulaire de connexion
 * @constructor
 */
function SigninButton() {
    return (
      <Link to="/signin" ><Button variant="contained" color="primary">
        Sign In
      </Button>
      </Link>
    );
}

export default SigninButton;
