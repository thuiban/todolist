import SwaggerUI from 'swagger-ui-react';
import '../App.css';
import React from "react";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
   swagger: {
       width : '100%'
   }
}));

/**
 * @desc Affiche la documentation de l'API
 * @constructor
 */
function Swagger() {
    const classes = useStyles()
    return (
        <div className={classes.swagger}>
            <SwaggerUI url={process.env.REACT_APP_SWAGGER_URL}/>
        </div>

    )
}

export default Swagger;
