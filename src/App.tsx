import React, {Component} from 'react';
import './App.css';
import {
    Route,
    Switch,
    Redirect
} from 'react-router-dom';

import Signin from "./views/Authentication/Signin";
import Signup from "./views/Authentication/Signup";
import UserDashboard from "./views/Manager/Dashboard";
import AdminDashboard from "./views/Admin/AdminDashboard";
import LostPassword from "./views/Authentication/LostPassword";
import Footer from "./app/Footer";
import Header from "./app/Header";
import ReactNotification from 'react-notifications-component';
import Swagger from "./app/Swagger";
import NavbarComponent from "./views/NavbarComponent";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

require('dotenv').config();

class App extends Component {
    constructor(props: any) {
        super(props);
        this.state = {
            response: 0,
            time: 0,
        };
    }

    PrivateRoute({component: Component, ...rest}) {
        return (
            <Route
                {...rest}
                render={(props) => localStorage.getItem('bearer') != null ? <Component {...props}/> :
                    <Redirect to={{pathname: '/signin', state: {from: props.location}}}/>}/>
        )
    }

    render() {
        return (
            <div className="GeneralStyle">
                <div className="App">
                    <Header/>
                </div>

                <div className="App-intro">
                    <NavbarComponent/>
                    <div className="App-content">
                        <Switch>
                            <Route path="/signin" component={Signin}/>
                            <Route path="/signup" component={Signup}/>
                            <this.PrivateRoute path="/dashboard" component={UserDashboard}/>
                            <this.PrivateRoute path="/admin/dashboard" component={AdminDashboard}/>
                            <this.PrivateRoute path="/swagger" component={Swagger}/>
                            <Route path="/forgot" component={LostPassword}/>
                            <Redirect to="/signin"/>
                        </Switch>

                    </div>
                    <div className="app-container">
                        <ReactNotification/>
                    </div>
                </div>
                <div className="App-footer">
                    <Footer/>
                </div>
            </div>
        );
    }
}

export default App;
